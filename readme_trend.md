### Data Transformations

Data is shown both as an interactive graphic and a table, and can be transformed as: 
  - Counts (raw data). 
  - Year-over-year (YoY) changes &ndash; change from *same month previous year*, as percentage.
  - Month-over-month (MoM) changes &ndash; change from the *previous month*, as percentage.

For spatial analysis, click *Spatial Analysis* in the top panel.

Note that the counts for different crime categories and different area units can vary greatly. Counts for individual communities are often too low and contain too many missing values to speak about changes as trends, and thus communities were excluded from trends analysis. Instead, aggregate counts for the whole City of Calgary were included. 

For the same reason, you should be careful when interpreting major YoY and MoM swings in crime: depending on the area and category, large changes can be a function of overall low crime counts and do not necessarily represent an actual trend. For example, if you select *Street Robbery* category for *Ward&nbsp;1*, you will see that YoY and MoM changes range from -80% to +400%, but if you look at crime counts for street robberies in Ward&nbsp;1, you will realize that these changes likely result from street robberies staying in low single digits, so an increase or decrease of just one robbery will have a major impact on YoY and MoM changes: 

![](street_robberies_ward1.png)

### Extracting Trend Components

Trends are not always easy to see from a plot of raw data or from YoY and MoM changes. Besides, time series data contains other components such as seasonal patterns and random fluctuations ("white noise"). To extract a trend, check the *Extract trend* checkbox: 

![](extract_trend.png)

You will see that many crime categories follow a well-defined seasonal pattern, and will be able to look at seasonally adjusted data instead of observed data (raw counts).

Note that *STL decomposition* was chosen as one of the most versatile and robust methods for time series decomposition, however, it should not be expected to always perform perfectly. Since Calgary crime data consists of a large number of time series, you may occasionally see things such as signal leaking into the remainder component.
