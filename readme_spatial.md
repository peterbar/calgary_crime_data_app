### How to Use

Select data by administrative area unit, time range, crime category, and data transformation (*Show as*). Then click on the map to open pop-ups with detailed information for a community or a ward.

For trends analysis, click *Trend Analysis* in the top panel.

### Data Transformations

#### Per 1000 Residents

Communities with small populations &ndash; such as industrial communities and some smaller residential communities &ndash; may significantly distort visual representation of the per capita data. For example, if we have 3 crimes attributed to a community with a population of 10 people, it will have very high per capita crime (300 per 1000 residents). Such a community would appear dark brown, while other communities with high (but not extremely high) per capita crime counts would look almost identical to communities with low crime. Per capita crime counts for such a community would likely result from a combination of random chance (3 is not a large number) and small population. To address this, *Remove communities* slider will appear if you select community as your area unit and crime per 1000 residents as your data transformation. The slider will allow to remove communities with smaller populations (up to 2000) from the analysis:

![](remove_outliers.png)

To illustrate, move slider to zero.

#### Percent Change

This transformation calculates percentage change in crime counts between the first and the last months in the time range. For example, to find year-over-year change in *All Crime* for September&nbsp;2021, select time range: *September&nbsp;2020 &ndash; September&nbsp;2021*. To find month-over-month change, select two months next to each other, e.g. *August&nbsp;2021 &ndash; September&nbsp;2021*. Due to low counts for most crime categories, calculating percent change *for communities* is restricted to *All Crime*. Otherwise the map would mostly show missing values, as you need at least 1 or more crimes per category per community for both the first and the last month to be able to calculate change over time for this category. For *electoral wards*, counts are higher due to much larger populations and land areas of wards, so change over time can be calculated for all categories. 

Note also that high percent change values shown on the map should not be misinterpreted as long-term trends. High values are often a function of small overall crime counts per community, resulting in large percentage changes if there are just a few more (or less) crimes taking place. Some (although not all) of these changes can be random.

To explore trends, click *Trend Analysis* in the top panel.

### Download Data

The results of the analysis can be downloaded as interactive HTML web pages, static PNG plots, and CSV tables. Records containing missing values are removed when downloading as CSV. 
