# Downloaders UI
dl_ui <- function(id) {
  
  ns <- shiny::NS(id)
  
  tagList(
    downloadButton(outputId = ns("download_html"), 
                   label = "Save as web page"),
    p(HTML("<div style='margin-bottom:10px;'></div>")), # adds some space between buttons
    
    downloadButton(outputId = ns("download_png"), 
                   label = "Save as picture"),
    p(HTML("<div style='margin-bottom:10px;'></div>")),
    
    downloadButton(outputId = ns("download_csv"), 
                   label = "Save as dataset")
  )

}


# Downloader functions

# (!) Note these should *not* be built with moduleServer: I strongly suspect downloadHandler to
# deal with namespacing in some non-standard way.
# Note also that reactives should be passed as *expressions* and evaluated here.

downloadHTML <- function(input, output, app_state) {
  output$download_html <-
    downloadHandler(
      filename = function() { paste0(app_state$title(), ".html") },
      content = function(file) { 
        shinybusy::show_modal_spinner(spin = "atom", text = "Downloading...") # shiny-only alt:
        on.exit(shinybusy::remove_modal_spinner())  # showModal(modalDialog("Downloading...", footer = NULL))
        htmlwidgets::saveWidget(app_state$widget(), file = file) # on.exit(removeModal())
      }
    )
}


downloadPNG <- function(input, output, app_state) {
  output$download_png <-
    downloadHandler(
      filename = function() { paste0(app_state$title(), ".png") },
      content = function(file) {
        shinybusy::show_modal_spinner(spin = "atom", text = "Downloading...")
        on.exit(shinybusy::remove_modal_spinner())
        htmlwidgets::saveWidget(app_state$widget(), "temp.html", selfcontained = FALSE)
        webshot::webshot("temp.html", file = file, vheight = 850)
      }
    )
}


downloadCSV <- function(input, output, app_state) {
  output$download_csv <-
    downloadHandler(
      filename = function() { paste0(app_state$title(), ".csv") },
      content = function(file) {
        shinybusy::show_modal_spinner(spin = "atom", text = "Downloading...")
        on.exit(shinybusy::remove_modal_spinner())
        write_csv(app_state$dataset() |> st_drop_geometry() |> drop_na(), file = file)
      }
    )
}
