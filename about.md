### Data and Methodology

Crime data was attributed to electoral wards based on the coordinates of the *centroids* of communities where crimes took place: if certain crimes took place in the community and this community's centroid (center point) is located within a specific ward (i.e. within a multipolygon describing this ward's boundaries), these crimes will be attributed to that ward. This method of data attribution was chosen over the attribution by ward names, because Community Crime Statistics dataset, first, does not contains names of electoral wards, and second, does not provide the exact coordinates of crime locations to protect privacy.

Note also that crime counts are based on the most serious violation per incident, and domestic violence is excluded from the Community Crime Statistics dataset for privacy reasons. Click <a href="https://data.calgary.ca/Health-and-Safety/Community-Crime-Statistics/78gh-n26t" target="_blank">here</a> for more information about the dataset. 

Ward and community population counts are based on the 2019 Municipal Census (Calgary Civic Census). Thus, population data may be unavailable for some newly-created communities.

### Data Sources

Crime, population, and geospatial data for Calgary communities and electoral wards was retrieved from <a href="https://data.calgary.ca/" target="_blank">The City of Calgary’s Open Data Portal</a>. Specifically, the following datasets were used: 

  - <a href="https://data.calgary.ca/Base-Maps/City-Boundary/erra-cqp9" target="_blank">City Boundary</a>
  - <a href="https://data.calgary.ca/Base-Maps/Community-District-Boundaries/surr-xmvs" target="_blank">Community District Boundaries</a>
  - <a href="https://data.calgary.ca/Government/Ward-Boundaries-for-2021-October-18-General-Electi/bxgr-t8xb" target="_blank">Ward Boundaries for 2021 October 18 General Election</a>
  - <a href="https://data.calgary.ca/Demographics/Census-by-Community-2019/jpwd-iaas" target="_blank">Census by Community 2019</a>
  - <a href="https://data.calgary.ca/Health-and-Safety/Community-Crime-Statistics/78gh-n26t" target="_blank">Community Crime Statistics</a>

### License and Disclaimer

Click <a href="https://dataenthusiast.ca/wp-content/uploads/2021/11/LICENSE.txt" target="_blank">here</a> to read the license.

Contains information licensed under the <a href="https://data.calgary.ca/stories/s/Open-Calgary-Terms-of-Use/u45n-7awa" target="_blank">Open Government License – City of Calgary</a>.

<span style="font-family:Lato; font-size: 12px;">THE USE OF THE CITY OF CALGARY DATA IN THIS SOFTWARE DOES NOT CONSTITUTE AN ENDORSEMENT BY THE CITY OF CALGARY OF THIS SOFTWARE.</span>

<span style="font-family:Lato; font-size: 12px;">THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHOR OR COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OR CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THIS SOFTWARE OR THE USE OR OTHER DEALINGS IN THIS SOFTWARE.</span>
